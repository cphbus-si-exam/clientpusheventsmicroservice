var Pusher = require('pusher');
var bodyParser = require('body-parser')
const amqp = require('amqplib');

const config = require('./config')

var pusher = new Pusher({
  appId: config.appId,
  key: config.key,
  secret: config.secret,
  cluster: config.cluster,
  encrypted: true
});


amqp.connect(config.rabbitmq)
    .then(connection => connection.createChannel())
    .then(channel => {
        channel.assertQueue("client_push_events", {durable: false });

        channel.consume("client_push_events", function(msg) {
            let data = JSON.parse(msg.content.toString())

            console.log(
                data, 
                pusher.trigger(data.channel, data.event, data.data)
            )
        } , {
            noAck: true
        })
    })
    .catch(err => {
        console.log('error', err)
        process.exit(1)
   })




const express = require('express');
var bodyParser = require('body-parser')

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.post("/", (req, res) => {
    console.log(req.body); 
    console.log(
        pusher.trigger(req.body.channel, req.body.event, req.body.data)
    )
    res.json({status:true})
})

app.listen(3000)

console.log("listning on port 3000")