module.exports = {
    rabbitmq: process.env.rabbitmq,
    appId: process.env.appId,
    key: process.env.key,
    secret: process.env.secret,
    cluster: process.env.cluster
}